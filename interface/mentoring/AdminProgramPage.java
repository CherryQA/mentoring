package mentoring;

import org.openqa.selenium.By;

public class AdminProgramPage {
	public static final String programActionButton = "//button[@class='btn-mWidth btn-sm btn btn-default dropdown-toggle']";
	public static final String programEditButton = "//a[contains(text(),'Edit')]";
	public static String programTimeZoneField = "//div[@class='btn-group bootstrap-select show-menu-arrow open']";
	public static String programVietnamTimeZone = "//span[contains(text(),'VST, Vietnam Standard Time, GMT+7:00')]";
	public static String programStartDateField = "//input[@id='ProgramStartDate']";
	public static String programSelectStartDate = "//a[@class='ui-state-default ui-state-highlight ui-state-active']";


}
