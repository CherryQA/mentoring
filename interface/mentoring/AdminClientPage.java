package mentoring;

import org.openqa.selenium.By;

public class AdminClientPage {
	public static final String dynamicResultSelecter = "//*[@class='id_class' and contains(text(),'%s')]";
	public static final String dynamicResultSelecterLastName = "//*[@class='id_class sorting_1' and contains(text(),'%s')]";
	public static final String dynamicResultSelecterEmail = "//*[@class='id_class email' and contains(text(),'%s')]";
	
	
	public static String adminClientButton = "//span[contains(text(),'Clients')]";
	public static String clientCreateNewButton = "//a[contains(text(),'Create New')]";
	public static String clientName = "//input[@id='clientname']";
	public static String clientCreateInPopUp = "//a[@class='btn btn-success btn-block text-uppercase addClientConfirm']";
	public static String clientCreateConfirmButton = "//button[@class='btn btn-success btn-block text-uppercase createClient']";
	public static String createClientSuccessful = "//div[@id='flashMessage']";
	public static String setPassSuccessful = "//div[@class='form-group p0']";
	public static String championNameSuccessful = "//a[@class= 'dropdown-toggle']";

	public static String clientViewButton = "//a[contains(text(),' View')]";
	
	public static String clientChampionButton = "//a[contains(text(),'Champions')]";
	public static String championIsExist = "//span[contains(text(),'%s')]";
	public static By copyLink = By.xpath("//p[contains(text(),'Copyable link ')]/a");
//-----------------------------
	public static String addProgramName = "//input[@class='form-control text-input']";
	public static String createProgramConfirmButton = "//button[@class='btn btn-success btn-block text-uppercase submitProgramData']";
	public static String programLoginButton = "(//input[@id='login-btn'])[1]";
	public static String emailIsMissing = "(//span[contains(text(),' This field is required.')])[1]";
	public static String passwordIsMissing = "(//span[contains(text(),' This field is required.')])[2]";
	public static String emailInvalid = "//span[contains(text(),' Your email is required and must be in a valid format.')]";
	public static String passwordInvalid = "//span[contains(text(),'Must be between 8-16 characters with at least 2 of any of the following; capital letter, number, special character e.g. !#.$')]";
	public static String viewProgramButton = "//a[contains(text(),' View')]";
	public static String submitProgramButton = "//button[@class='btn btn-success btn-block text-uppercase submitProgramData']";
	public static String isProgramCreated = "//div[contains(text(),'Program created.')]";
	public static String isProgrameNameDisPlayedCorrect = "//span[contains(text(),'CrAutoProgram')]";
	

}
