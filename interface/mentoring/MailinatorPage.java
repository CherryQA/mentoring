package mentoring;

import org.openqa.selenium.By;

public class MailinatorPage {

	//==================================Static Controls========================================//
	public static By mailAddressTextfield = By.xpath("//input[@id='inboxfield']");
	public static By goButton = By.xpath("//button[contains(text(),'Go!')]");
	public static By mailFrame = By.xpath("//iframe[@id='publicshowmaildivcontent']");
	public static By emailAdress = By.xpath("//input[@id='identifierId']");
	public static By nextButton = By.xpath("//div[@id='identifierNext']");		
	public static By password = By.xpath("//input[@name='password']");
	public static By signInButton = By.xpath("//div[@id='passwordNext']");		
	public static By composeButton = By.xpath("//div[text()='COMPOSE']");
	public static By recipientTextarea = By.xpath("(//tr[contains(.,'To')]//textarea)[1]");
	public static By subjectBox = By.xpath("//input[@name='subjectbox']");
	public static By messageBody = By.xpath("//div[@aria-label='Message Body']");
	public static By attachFile = By.xpath("//div[@aria-label='Attach files']/div/div/div");
	public static By sendButton = By.xpath("//div[text()='Send']");
	public static By deleteEmail = By.xpath("(//div[@aria-label='Delete'])");
	public static By yopmailFrame = By.xpath("//iframe[@id='ifmail']");
	public static By inboxiFrame = By.xpath("//iframe[@id='ifinbox']");

	
	
	// ==================================Dynamic Controls========================================//
	public static String dynamicMailLink = "//div[contains(text(),'%s')]/..";
	public static String dynamicYopMailTitle = "//span[contains(text(),'%s')]";
	public static String expandButton = "//div[@data-tooltip='Show trimmed content']";
	public static String dynamicApproveLink = "//div[contains(text(),'%s')]//a[text()='%s']";
	
}
