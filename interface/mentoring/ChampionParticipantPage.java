package mentoring;

import org.openqa.selenium.By;

public class ChampionParticipantPage {
	
	public static String participantButton = "//a[contains(text(),'Participants')]";
	public static String participantChooseOption = "//span[contains(text(),'Choose option')]";
	public static String participantChooseMentor = "(//li[@data-original-index='1'])[1]";
	public static String participantChooseMentee = "(//li[@data-original-index='2'])[1]";
	public static String participantChooseMEntorAndMentee = "(//li[@data-original-index='3'])[1]";
	public static String participantMenteeAllowed = "//button[@data-id='mentee_allowed']";
//	public static String participantMenteeAllowed = "//button[@data-id='mentee_allowed']";
	public static String participantNumberMenteeAllowedIs5 = "//*[@data-original-index='4']";
	public static String participantSaveButton = "(//button[@type='submit'])[2]";

	public static String participantCreatedSuccessful = "//div[@id='flashMessage' and contains(text(),'The participant is created in the system / added to program')]";
	public static String isParticipantNameDisplayedCorrect = "//*[@class='helBold font28'and contains(text(),'%s')]";
	public static String isParticipantStatusDisplayedCorrect = "//dd[contains(text(),'Active Unmatch')]";
	public static String isParticipantRoleDisplayedCorrect = "//dd[contains(text(),'Mentor')]";
	public static String isParticipantAddDateDisplayedCorrect = "//dd[contains(text(),'currentDate')]";
	public static String isParticipantMentorTrainingStatussDisplayedCorrect = "//dd[contains(text(),'Incomplete')]";
	public static String isParticipantMenteeTrainingStatussDisplayedCorrect = "//dd[contains(text(),'NA')]";
	public static String isParticipantEmailNotifyDisplayedCorrect = "//dd[contains(text(),'On')]";
	public static String isApplicationNotifyDisplayed = "//*[@class='fa fa-exclamation-circle']";
	public static String isMentorApplicationTileDisplayed = "//*[contains(text(),'My Mentor Application')]";
	public static String isMenteeApplicationTileDisplayed = "//*[contains(text(),'My Mentee Application')]";

}
