package LoginApplications;

import org.testng.annotations.Test;

import common.AbstractTest;
import common.Common;
import common.DriverManager;
import page.LoginAdminPage;
import page.MailinatorPage;
import page.PageFactory;
import page.ProgramPage;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.awt.AWTException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

public class TC_003_AddParticipantAccountWithoutApplication extends AbstractTest {
	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {
		openBrowser(browser, port, ipClient);

		loginAdminPage = PageFactory.getLoginAdminPage(DriverManager.getDriver(), ipClient);
		programPage = PageFactory.getProgramPage(DriverManager.getDriver(), ipClient);
		mailinatorPage = PageFactory.getMailinatorPage(DriverManager.getDriver(), ipClient);

		emailAdmin = "cherry.seniorqa@gmail.com";
		championDefaultEmail = "cherry.automation001@yopmail.com";
		password = "Abcd1234";
		clientName = "CherryAuto";
		clientPage = "https://staging.artofmentoring.net/admin/clients/";
		programAddress = "https://cherryauto.staging.artofmentoring.net/champion/Users/login?clientUrl=cherryauto&programName=crautoprogram";
		programName = "CrAutoProgram";

		participantFirstName = "Cherry" + getUniqueText(3);
		participantLastName = getUniqueText(4);
		participantEmail = participantFirstName + participantLastName + "@yopmail.com";

		currentDate = Common.getCommon().getCurrentDay() + "/" + Common.getCommon().getCurrentMonth() + "/"
				+ Common.getCommon().getCurrentYear();

		// championFirstName = "Cherry" + getUniqueNumber(5);
		// championLastName = "Automation" + getUniqueNumber(5);
		// championEmail = "Cherry.Automation" + getUniqueNumber(5) +
		// "@yopmail.com";
		// championPhoneNumber = getUniqueNumber(5) + getUniqueNumber(5);

	}

	@Test(groups = { "regression" }, description = "Create Client", enabled = true)

	public void TC_003_AddParticipantAccountWithoutApplication_001_AddMentorAccout() throws AWTException {
		// login to admin page
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 01: Open the site mentoring");
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 03: Open Program page");
		loginAdminPage.openLink(DriverManager.getDriver(), programAddress);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 04: Cick on login button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.LoginAdminPage.championlogInbutton);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 05: Input champion email");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminEmail", championDefaultEmail);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 06: Input Champion password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminPassword", password);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 07: Cick on login button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");
		loginAdminPage.sleep(1);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 08: Cick on Participant Section");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(), "fa fa-cParticipants");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 09: Select Participant page");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantButton);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 10: Click On Add button");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(),
				"btn-mWidth btn-sm btn btn-success dropdown-toggle");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 11: Click On Add Individual button");
		programPage.clickOnElementByItsText(DriverManager.getDriver(), "Add Individual");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 12: Click On Choose Mentor option");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantChooseOption);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 12: Click On Choose Mentor option");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantChooseMentor);
		programPage.clickOnElementByItsName(DriverManager.getDriver(), "qqfile");
		programPage.sleep(1);
		programPage.uploadImage(DriverManager.getDriver(), "abcimage");
		programPage.sleep(1);
		programPage.clickOnElementByItsID(DriverManager.getDriver(), "jqi_state0_buttonDone");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 13: Input First Name");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "firstname", participantFirstName);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 14: Input Last Name");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "lastname", participantLastName);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 15: Input Email");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "email", participantEmail);

		JavascriptExecutor jse6 = (JavascriptExecutor) DriverManager.getDriver();
		jse6.executeScript("window.scrollBy(0,250)", "");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 16: Click on Mentee Allowed Field");
		programPage.forceClick(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantMenteeAllowed);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 17: Select Number of Mentee is 5");
		programPage.click(DriverManager.getDriver(),
				mentoring.ChampionParticipantPage.participantNumberMenteeAllowedIs5);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 18: Select Save button");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantSaveButton);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 19:Verify participant informations");
		verifyTrue(programPage.isParticipantCreatedSuccessful());
		verifyTrue(programPage.isPariticpantNameDisplayedCorrect(participantFirstName + " " + participantLastName));
		verifyTrue(programPage.isParticipantStatusDisplayedCorrect());
		verifyTrue(programPage.isParticipantRoleDisplayedCorrect());
		verifyTrue(programPage.isParticipantAddDateDisplayedCorrect());
		verifyTrue(programPage.isParticipantMentorTrainingStatussDisplayedCorrect());
		verifyTrue(programPage.isParticipantMenteeTrainingStatussDisplayedCorrect());
		verifyTrue(programPage.isParticipantEmailNotifyDisplayedCorrect());

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 20:Open Yopmail");
		programPage.openLink(DriverManager.getDriver(), "http://www.yopmail.com/en/");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 21: Input email");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "login", participantEmail);

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 22: Click on Check Inbox");
		programPage.clickOnElementByItsValue(DriverManager.getDriver(), "Check Inbox");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 23: Open Register email");
		mailinatorPage.clickOnEmailTitle(DriverManager.getDriver(),"CherryAuto HAS INVITED YOU TO PARTICIPATE IN THE CrAutoProgram");

		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 24: Click Access Portal button");
		emaildriver = mailinatorPage.clickOnlinkInsideMail(DriverManager.getDriver(),"Access Portal" );		
		programPage.sleep();
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 25: Input password");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "password", password);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 26: Select policy 01");
		programPage.clickOnElementByItsText(DriverManager.getDriver(), "I agree to the collection, use and storage of my personal information for this program");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 27: Select policy 02");
		programPage.clickOnElementByItsID(DriverManager.getDriver(), "terms_acceptable");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 28: Click on Continue button");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(), "btn btn-primary form-custom-btn btn-common secondary-bg-color");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 28: Click on Home button");
		programPage.clickOnDivByItsText(DriverManager.getDriver(), "Home");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_001 - 29: Verify pasrticipant login success and Notification is displayed on Application tile");		
		verifyTrue(programPage.isApplicationNotifyDisplayed());
		verifyTrue(programPage.isMentorApplicationTileDisplayed());
		
		programPage.logout();

	}
	@Test(groups = { "regression" }, description = "Create Client", enabled = true)
	
	public void TC_003_AddParticipantAccountWithoutApplication_002_AddMenteeAccout() throws AWTException {
		// login to admin page
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 01: Open the site mentoring");
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 03: Open Program page");
		loginAdminPage.openLink(DriverManager.getDriver(), programAddress);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 04: Cick on login button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.LoginAdminPage.championlogInbutton);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 05: Input champion email");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminEmail", championDefaultEmail);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 06: Input Champion password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminPassword", password);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 07: Cick on login button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");
		loginAdminPage.sleep(1);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 08: Cick on Participant Section");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(), "fa fa-cParticipants");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 09: Select Participant page");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantButton);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 10: Click On Add button");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(),
				"btn-mWidth btn-sm btn btn-success dropdown-toggle");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 11: Click On Add Individual button");
		programPage.clickOnElementByItsText(DriverManager.getDriver(), "Add Individual");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 12: Click On Choose Mentor option");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantChooseOption);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 12: Click On Choose Mentee option");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantChooseMentee);
		programPage.clickOnElementByItsName(DriverManager.getDriver(), "qqfile");
		programPage.sleep(1);
		programPage.uploadImage(DriverManager.getDriver(), "abcimage");
		programPage.sleep(1);
		programPage.clickOnElementByItsID(DriverManager.getDriver(), "jqi_state0_buttonDone");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 13: Input First Name");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "firstname", participantFirstName);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 14: Input Last Name");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "lastname", participantLastName);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 15: Input Email");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "email", participantEmail);
		
		JavascriptExecutor jse6 = (JavascriptExecutor) DriverManager.getDriver();
		jse6.executeScript("window.scrollBy(0,250)", "");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 16: Click on Mentee Allowed Field");
		programPage.forceClick(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantMenteeAllowed);
		
//		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 17: Select Number of Mentee is 5");
//		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantNumberMenteeAllowedIs5);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 18: Select Save button");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantSaveButton);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 19:Verify participant informations");
		verifyTrue(programPage.isParticipantCreatedSuccessful());
		verifyTrue(programPage.isPariticpantNameDisplayedCorrect(participantFirstName + " " + participantLastName));
		verifyTrue(programPage.isParticipantStatusDisplayedCorrect());
		verifyTrue(programPage.isParticipantRoleDisplayedCorrect());
		verifyTrue(programPage.isParticipantAddDateDisplayedCorrect());
		verifyTrue(programPage.isParticipantMentorTrainingStatussDisplayedCorrect());
		verifyTrue(programPage.isParticipantMenteeTrainingStatussDisplayedCorrect());
		verifyTrue(programPage.isParticipantEmailNotifyDisplayedCorrect());
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 20:Open Yopmail");
		programPage.openLink(DriverManager.getDriver(), "http://www.yopmail.com/en/");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 21: Input email");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "login", participantEmail);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 22: Click on Check Inbox");
		programPage.clickOnElementByItsValue(DriverManager.getDriver(), "Check Inbox");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 23: Open Register email");
		mailinatorPage.clickOnEmailTitle(DriverManager.getDriver(),"CherryAuto HAS INVITED YOU TO PARTICIPATE IN THE CrAutoProgram");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 24: Click Access Portal button");
		emaildriver = mailinatorPage.clickOnlinkInsideMail(DriverManager.getDriver(),"Access Portal" );		
		programPage.sleep();
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 25: Input password");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "password", password);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 26: Select policy 01");
		programPage.clickOnElementByItsText(DriverManager.getDriver(), "I agree to the collection, use and storage of my personal information for this program");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 27: Select policy 02");
		programPage.clickOnElementByItsID(DriverManager.getDriver(), "terms_acceptable");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 28: Click on Continue button");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(), "btn btn-primary form-custom-btn btn-common secondary-bg-color");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 28: Click on Home button");
		programPage.clickOnDivByItsText(DriverManager.getDriver(), "Home");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 29: Verify pasrticipant login success and Notification is displayed on Application tile");		
		verifyTrue(programPage.isApplicationNotifyDisplayed());
		verifyTrue(programPage.isMentorApplicationTileDisplayed());		
	}
	@Test(groups = { "regression" }, description = "Create Client", enabled = true)
	
	public void TC_003_AddParticipantAccountWithoutApplication_003_AddBothMentorAndMenteeeAccout() throws AWTException {
		// login to admin page
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 01: Open the site mentoring");
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 03: Open Program page");
		loginAdminPage.openLink(DriverManager.getDriver(), programAddress);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 04: Cick on login button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.LoginAdminPage.championlogInbutton);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 05: Input champion email");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminEmail", championDefaultEmail);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 06: Input Champion password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminPassword", password);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 07: Cick on login button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");
		loginAdminPage.sleep(1);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 08: Cick on Participant Section");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(), "fa fa-cParticipants");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 09: Select Participant page");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantButton);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 10: Click On Add button");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(),
				"btn-mWidth btn-sm btn btn-success dropdown-toggle");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 11: Click On Add Individual button");
		programPage.clickOnElementByItsText(DriverManager.getDriver(), "Add Individual");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 12: Click On Choose Mentor option");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantChooseOption);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 12: Click On Choose Mentee option");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantChooseMEntorAndMentee);
		programPage.clickOnElementByItsName(DriverManager.getDriver(), "qqfile");
		programPage.sleep(1);
		programPage.uploadImage(DriverManager.getDriver(), "abcimage");
		programPage.sleep(1);
		programPage.clickOnElementByItsID(DriverManager.getDriver(), "jqi_state0_buttonDone");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 13: Input First Name");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "firstname", participantFirstName);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 14: Input Last Name");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "lastname", participantLastName);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 15: Input Email");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "email", participantEmail);
		
		JavascriptExecutor jse6 = (JavascriptExecutor) DriverManager.getDriver();
		jse6.executeScript("window.scrollBy(0,250)", "");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 16: Click on Mentee Allowed Field");
		programPage.forceClick(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantMenteeAllowed);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 17: Select Number of Mentee is 5");
		programPage.click(DriverManager.getDriver(),
				mentoring.ChampionParticipantPage.participantNumberMenteeAllowedIs5);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 18: Select Save button");
		programPage.click(DriverManager.getDriver(), mentoring.ChampionParticipantPage.participantSaveButton);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 19:Verify participant informations");
		verifyTrue(programPage.isParticipantCreatedSuccessful());
		verifyTrue(programPage.isPariticpantNameDisplayedCorrect(participantFirstName + " " + participantLastName));
		verifyTrue(programPage.isParticipantStatusDisplayedCorrect());
		verifyTrue(programPage.isParticipantRoleDisplayedCorrect());
		verifyTrue(programPage.isParticipantAddDateDisplayedCorrect());
		verifyTrue(programPage.isParticipantMentorTrainingStatussDisplayedCorrect());
		verifyTrue(programPage.isParticipantMenteeTrainingStatussDisplayedCorrect());
		verifyTrue(programPage.isParticipantEmailNotifyDisplayedCorrect());
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 20:Open Yopmail");
		programPage.openLink(DriverManager.getDriver(), "http://www.yopmail.com/en/");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 21: Input email");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "login", participantEmail);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 22: Click on Check Inbox");
		programPage.clickOnElementByItsValue(DriverManager.getDriver(), "Check Inbox");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 23: Open Register email");
		mailinatorPage.clickOnEmailTitle(DriverManager.getDriver(),"CherryAuto HAS INVITED YOU TO PARTICIPATE IN THE CrAutoProgram");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 24: Click Access Portal button");
		emaildriver = mailinatorPage.clickOnlinkInsideMail(DriverManager.getDriver(),"Access Portal" );		
		programPage.sleep();
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 25: Input password");
		programPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "password", password);
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 26: Select policy 01");
		programPage.clickOnElementByItsText(DriverManager.getDriver(), "I agree to the collection, use and storage of my personal information for this program");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 27: Select policy 02");
		programPage.clickOnElementByItsID(DriverManager.getDriver(), "terms_acceptable");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 28: Click on Continue button");
		programPage.clickOnElementByItsClass(DriverManager.getDriver(), "btn btn-primary form-custom-btn btn-common secondary-bg-color");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 28: Click on Home button");
		programPage.clickOnDivByItsText(DriverManager.getDriver(), "Home");
		
		log.info("Step TC_003_AddParticipantAccountWithoutApplication_002 - 29: Verify pasrticipant login success and Notification is displayed on Application tile");		
		verifyTrue(programPage.isApplicationNotifyDisplayed());
		verifyTrue(programPage.isMentorApplicationTileDisplayed());		
		verifyTrue(programPage.isMentorApplicationTileDisplayed());		

	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		// closeBrowser();
	}

	private LoginAdminPage loginAdminPage;
	private ProgramPage programPage;
	private MailinatorPage mailinatorPage;
	private WebDriver emaildriver;

	String emailAdmin, password;
	String clientName, clientPage, programAddress, championDefaultEmail;
	String championFirstName, championLastName, championEmail, championPhoneNumber, currentDate;
	String userProfileName, programName;
	String participantFirstName, participantLastName, participantEmail;
}
