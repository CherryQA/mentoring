package LoginApplications;

import org.testng.annotations.Test;

import common.AbstractTest;
import common.DriverManager;
import page.LoginAdminPage;
import page.MailinatorPage;
import page.PageFactory;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

public class TC_002_LoginChampionAccout extends AbstractTest {
	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {
		openBrowser(browser, port, ipClient);

		loginAdminPage = PageFactory.getLoginAdminPage(DriverManager.getDriver(), ipClient);
		mailinatorPage = PageFactory.getMailinatorPage(DriverManager.getDriver(), ipClient);

		emailAdmin = "cherry.seniorqa@gmail.com";
		championDefaultEmail = "cherry.automation001@yopmail.com";
		password = "Abcd1234";
		clientName = "CherryAuto";
		clientPage = "https://staging.artofmentoring.net/admin/clients/";
		programPage = "https://cherryauto.staging.artofmentoring.net/champion/Users/login?clientUrl=cherryauto&programName=crautoprogram";
		programName = "CrAutoProgram";

		championFirstName = "Cherry" + getUniqueNumber(5);
		championLastName = "Automation" + getUniqueNumber(5);
		championEmail = "Cherry.Automation" + getUniqueNumber(5) + "@yopmail.com";
		championPhoneNumber = getUniqueNumber(5) + getUniqueNumber(5);

	}

	@Test(groups = { "regression" }, description = "Create Client", enabled = true)
	public void LoginChampionAccout_001_LoginAdmin() {
		// login to admin page
		log.info("Step LoginChampionAccout_001 - 01: Open the site mentoring");
		log.info("Step LoginChampionAccout_001 - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);
		loginAdminPage.createNewClient(clientName);
	}

	@Test(groups = { "regression" }, description = "Create Client", enabled = false)
	public void LoginChampionAccout_002_CreateChampionAccount() {
		// login to admin page
		log.info("Step LoginChampionAccout_002 - 01: Open the site mentoring");
		log.info("Step LoginChampionAccout_002 - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);
		loginAdminPage.createNewClient(clientName);

		// // loginAdminPage.createNewChampion(clientName, championFirstName,
		// // championLastName, championEmail, championPhoneNumber);
		//
		// // log.info("Step LoginChampionAccout_002 - 27: Open yopmail");
		// //
		// loginAdminPage.openLink(DriverManager.getDriver(),"http://www.yopmail.com/en/");
		// //
		// // log.info("Step LoginChampionAccout_002 - 28: Input email");
		// //
		// loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(),
		// // "login", "cherry.automation14217@yopmail.com");
		// //
		// // log.info("Step LoginChampionAccout_002 - 29: Click on Check
		// Inbox");
		// // loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(),
		// // "Check Inbox");
		// //
		// // log.info("Step LoginChampionAccout_002 - 30: Open Register
		// email");
		// // mailinatorPage.clickOnEmailTitle(DriverManager.getDriver(),"been
		// // invited to join the Art of Mentoring platform");
		// //
		// // mailinatorPage.switchToFrame(DriverManager.getDriver(),
		// // mentoring.MailinatorPage.yopmailFrame);
		// //
		// // String copylink =
		// loginAdminPage.getText(DriverManager.getDriver(),
		// // mentoring.AdminClientPage.copyLink);
		// // loginAdminPage.openLink(DriverManager.getDriver(),copylink);

		log.info("Step LoginChampionAccout_002 - 09: Open Client page");
		loginAdminPage.openLink(DriverManager.getDriver(), clientPage);

		log.info("Step LoginChampionAccout_002 - 10: Select filter client name ");
		loginAdminPage.inputSelecterTextfieldByColumn(DriverManager.getDriver(), "0", clientName);

		log.info("Step LoginChampionAccout_002 - 11: Click on View button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientViewButton);

		log.info("Step LoginChampionAccout_002 - 12: Click on Champion button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientChampionButton);

		log.info("Step LoginChampionAccout_002 - 13: Click on Create New button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateNewButton);
		loginAdminPage.sleep(1);

		log.info("Step LoginChampionAccout_002 - 14: Input Champion First Name ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "firstname", championFirstName);

		log.info("Step LoginChampionAccout_002 - 15: Input Champion Last Name ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "lastname", championLastName);

		log.info("Step LoginChampionAccout_002 - 16: Input Champion Email ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "email", championEmail);

		log.info("Step LoginChampionAccout_002 - 17: Input Champion Phone Number ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "phone", championPhoneNumber);

		log.info("Step LoginChampionAccout_002 - 18: Click on Create button ");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "Create");

		log.info("Step LoginChampionAccout_002 - 26: Log out");
		loginAdminPage.logout();

		log.info("Step LoginChampionAccout_002 - 27: Open yopmail");
		loginAdminPage.openLink(DriverManager.getDriver(), "http://www.yopmail.com/en/");

		log.info("Step LoginChampionAccout_002 - 28: Input email");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "login", championEmail);

		log.info("Step LoginChampionAccout_002 - 29: Click on Check Inbox");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "Check Inbox");

		log.info("Step LoginChampionAccout_002 - 30: Open Register email");
		mailinatorPage.clickOnEmailTitle(DriverManager.getDriver(),
				"been invited to join the Art of Mentoring platform");

		log.info("Step LoginChampionAccout_002 - 31: Click Set password button");
		emaildriver = mailinatorPage.clickOnlinkInsideMail(DriverManager.getDriver(),"Set password");

		log.info("Step LoginChampionAccout_002 - 32: Input password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "password", password);

		log.info("Step LoginChampionAccout_002 - 33: Confirm password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "UserConfirmPassword", password);

		log.info("Step LoginChampionAccout_002 - 34: Select checkbox");
		loginAdminPage.clickOnElementByItsID(DriverManager.getDriver(), "keepSigned5");

		log.info("Step LoginChampionAccout_002 - 35: Click Login");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");

		log.info("Step LoginChampionAccout_002 - 36: Click OK button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "OK");
		loginAdminPage.sleep();

		log.info("Step LoginChampionAccout_002 - 37: Verify Champion Account is Created");
		verifyTrue(loginAdminPage.isChampionNameCorrect(DriverManager.getDriver(), userProfileName));

		log.info("Step LoginChampionAccout_002 - 38: Log out");
		loginAdminPage.logout();

		log.info("Step LoginChampionAccout_002 - 39: Click On Login Button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.programLoginButton);

		log.info("Step LoginChampionAccout_002 - 40: Verify email and password are missing");
		verifyTrue(loginAdminPage.isEmailisMissing());
		verifyTrue(loginAdminPage.isPassWordIsMissing());

	}

	@Test(groups = { "regression" }, description = "Login with  email address is invalid", enabled = true)
	public void LoginChampionAccout_003_LoginWithEmailIsInvalid() {
		// login to admin page
		log.info("Step LoginChampionAccout_003 - 01: Open the site mentoring");
		loginAdminPage.openLink(DriverManager.getDriver(), programPage);

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.LoginAdminPage.championlogInbutton);

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminEmail", "cherry.qa");

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		verifyTrue(loginAdminPage.isEmailInvalid());

	}

	@Test(groups = { "regression" }, description = "Login with  email address is invalid", enabled = true)
	public void LoginChampionAccout_004_LoginWithPassIsInvalid() {
		// login to admin page
		log.info("Step LoginChampionAccout_003 - 01: Open the site mentoring");
		loginAdminPage.openLink(DriverManager.getDriver(), programPage);

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.LoginAdminPage.championlogInbutton);

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminEmail", "cherry.seniorqa@gmail.com");

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminPassword", "779456656");

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");

		log.info("Step LoginChampionAccout_003 - 02: Lick on login button");
		verifyTrue(loginAdminPage.isPasswordInvalid());

	}

	@Test(groups = { "regression" }, description = "Login with  email and password correct", enabled = true)
	public void LoginChampionAccout_005_LoginWithPassandEmailCorrect() {
		// login to admin page
		log.info("Step LoginChampionAccout_005 - 01: Open the site mentoring");
		loginAdminPage.openLink(DriverManager.getDriver(), programPage);

		log.info("Step LoginChampionAccout_005 - 02: Cick on login button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.LoginAdminPage.championlogInbutton);

		log.info("Step LoginChampionAccout_005 - 03: Input champion email");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminEmail", championDefaultEmail);

		log.info("Step LoginChampionAccout_005 - 04: Input Champion password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "adminPassword", password);

		log.info("Step LoginChampionAccout_005 - 05: Cick on login button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");
		loginAdminPage.sleep(1);

		log.info("Step LoginChampionAccout_005 - 06: Verify Champion login successful");
//		verifyEquals(loginAdminPage.getTextfieldByClass(DriverManager.getDriver(), "helBold"), programName);
		verifyTrue(loginAdminPage.isProgrameNameDisPlayedCorrect());
		
		loginAdminPage.logout();

	}

	@Test(groups = { "regression" }, description = "Add New Program TO Client", enabled = false)
	public void LoginChampionAccout_006_AddChampionToProgram() {
		log.info("Step LoginChampionAccout_003 - 01: Open Program Page");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.openLink(DriverManager.getDriver(), "https://staging.artofmentoring.net/admin/programs/");

		log.info("Step LoginChampionAccout_003 - 02: Click Create New Program Button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateNewButton);

		log.info("Step LoginChampionAccout_003 - 03: Select Client Name");
		loginAdminPage.selectItemFromDropdownByClass(DriverManager.getDriver(), "selectpicker show-menu-arrow",
				clientName);

		log.info("Step LoginChampionAccout_003 - 04: Input Program Name");
		loginAdminPage.inputSelecterTextfieldByClass(DriverManager.getDriver(), "form-control text-input", programName);

		log.info("Step LoginChampionAccout_003 - 05: Select Dropdown is YES");
		loginAdminPage.selectItemFromDropdownByClassAndIndex(DriverManager.getDriver(), "selectpicker", "Yes");

		log.info("Step LoginChampionAccout_003 - 06: Click On Create Button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "Create");

		log.info("Step LoginChampionAccout_003 - 06: Click On Create Button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.submitProgramButton);
		loginAdminPage.sleep(1);
		verifyTrue(loginAdminPage.isProgrameCreated());

		// Search Program
		// log.info("Step LoginChampionAccout_003 - 06: Search Program");
		// loginAdminPage.inputSelecterTextfieldByColumn(DriverManager.getDriver(),
		// "1", programName);
		// loginAdminPage.sleep(1);
		// log.info("Step LoginChampionAccout_003 - 06: Click on View button");
		// loginAdminPage.click(DriverManager.getDriver(),
		// mentoring.AdminClientPage.viewProgramButton);
		// Edit Program

		log.info("Step LoginChampionAccout_003 - 06: Click On Action Button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminProgramPage.programActionButton);

		log.info("Step LoginChampionAccout_003 - 06: Click On Edit Button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminProgramPage.programEditButton);
		loginAdminPage.sleep(1);
		log.info("Step LoginChampionAccout_003 - 06: Click On TimeZone Field");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminProgramPage.programTimeZoneField);

		log.info("Step LoginChampionAccout_003 - 06: Select VietNam TimeZone");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminProgramPage.programVietnamTimeZone);

		log.info("Step LoginChampionAccout_003 - 06: Select Start Date Field");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminProgramPage.programStartDateField);

		log.info("Step LoginChampionAccout_003 - 06: Select Start Date");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminProgramPage.programSelectStartDate);

	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		// closeBrowser();
	}

	private LoginAdminPage loginAdminPage;
	private MailinatorPage mailinatorPage;
	private WebDriver emaildriver;

	String emailAdmin, password;
	String clientName, clientPage, programPage, championDefaultEmail;
	String championFirstName, championLastName, championEmail, championPhoneNumber, currentDate;
	String userProfileName, programName;
}
