package LoginApplications;

import org.testng.annotations.Test;

import common.AbstractTest;
import common.DriverManager;
import page.LoginAdminPage;
import page.MailinatorPage;
import page.PageFactory;
import common.AbstractTest;
import common.Constant;
import common.Common;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class TC_001_SignUpChampionAccount extends AbstractTest {
	
	@Parameters({ "browser", "ipClient", "port" })
	@BeforeClass(alwaysRun = true)
	public void setup(String browser, String ipClient, String port) {

		openBrowser(browser, port, ipClient);

		loginAdminPage = PageFactory.getLoginAdminPage(DriverManager.getDriver(), ipClient);
		mailinatorPage = PageFactory.getMailinatorPage(DriverManager.getDriver(), ipClient);

		
		emailAdmin = "cherry.seniorqa@gmail.com";
		password = "Abcd1234";
		clientName = "CherryAuto";
		clientPage = "https://staging.artofmentoring.net/admin/clients/" ; 
		championFirstName = "Cherry" + getUniqueNumber(5) ; 
		championLastName = "Automation" + getUniqueNumber(5); 
		championEmail = "Cherry.Automation" + getUniqueNumber(5) + "@yopmail.com" ; 
		championPhoneNumber = getUniqueNumber(5)+ getUniqueNumber(5);
//		currentDate = Common.getCommon().getCurrentDay()+"/"+Common.getCommon().getCurrentMonth()+"/"+Common.getCommon().getCurrentYear();
		userProfileName = "Hello" + championFirstName ;
	} 
	
	@Test(groups = { "regression" }, description = "Create Client", enabled = true)
	public void Admin_001_LoginAdmin() {
		// login to admin page
		log.info("Step TC_001_CreateClient - 01: Open the site mentoring");
		log.info("Step TC_001_CreateClient - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);
		loginAdminPage.createNewClient(clientName);

	}
	@Test(groups = { "regression" }, description = "Create Client", enabled = false)
	public void Admin_002_CreateClient() {
		// login to admin page
		log.info("Step TC_001_CreateClient - 01: Open the site mentoring");
		log.info("Step TC_001_CreateClient - 02: Input Admin Email and Password");
		loginAdminPage.login(emailAdmin, password);
		loginAdminPage.sleep(3);
		// Creat a Client
		log.info("Step TC_001_CreateClient - 03: Click in Client button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.adminClientButton);
		
		log.info("Step TC_001_CreateClient - 04: Click in Create New button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateNewButton);
		
		log.info("Step TC_001_CreateClient - 05: Input Client Name");
		loginAdminPage.inputTextfieldByID(DriverManager.getDriver(), "clientname", clientName);
		
		log.info("Step TC_001_CreateClient - 06: Select Client Index");
		loginAdminPage.selectItemFromDropdownByID(DriverManager.getDriver(), "ClientIndexed" , "Yes");
		
		log.info("Step TC_001_CreateClient - 07: Click in Client button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateInPopUp);
		
		log.info("Step TC_001_CreateClient - 08: Click in Client Confirm button");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateConfirmButton);
		
		log.info("Step TC_001_CreateClient - 09: Verify Client Create successfull");
		verifyTrue(loginAdminPage.isClientSuccessMessDisplay(DriverManager.getDriver(), "Client has been added successfully."));
	

	}
	
	@Test(groups = { "regression" }, description = "CreateChampionOnClient", enabled = true)
	public void Admin_003_CreateChampionOnClient() {
		log.info("Step TC_001_CreateClient - 01: Open the site mentoring");
		log.info("Step TC_001_CreateClient - 02: Input Admin Email and Password");
		log.info("Step TC_001_CreateClient - 03: Click in Client button");
		log.info("Step TC_001_CreateClient - 04: Click in Create New button");
		log.info("Step TC_001_CreateClient - 05: Input Client Name");
		log.info("Step TC_001_CreateClient - 06: Select Client Index");
		log.info("Step TC_001_CreateClient - 07: Click in Client button");
		log.info("Step TC_001_CreateClient - 08: Click in Client Confirm button");
		
		log.info("Step TC_001_CreateClient - 09: Open Client page");
		loginAdminPage.openLink(DriverManager.getDriver(), clientPage);
		
		log.info("Step TC_001_CreateClient - 10: Select filter client name ");
		loginAdminPage.inputSelecterTextfieldByColumn(DriverManager.getDriver(), "0", clientName);
		
		log.info("Step TC_001_CreateClient - 11: Click on View button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientViewButton);
		
		log.info("Step TC_001_CreateClient - 12: Click on Champion button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientChampionButton);
		
		log.info("Step TC_001_CreateClient - 13: Click on Create New button ");
		loginAdminPage.click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateNewButton);
		loginAdminPage.sleep(1);
		
		log.info("Step TC_001_CreateClient - 14: Input Champion First Name ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "firstname", championFirstName);
		
		log.info("Step TC_001_CreateClient - 15: Input Champion Last Name ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "lastname", championLastName);
		
		log.info("Step TC_001_CreateClient - 16: Input Champion Email ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "email", championEmail);
		
		log.info("Step TC_001_CreateClient - 17: Input Champion Phone Number ");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "phone", championPhoneNumber);
		
		log.info("Step TC_001_CreateClient - 18: Click on Create button ");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "Create");
		
		log.info("Step TC_001_CreateClient - 19: Verify Champion Create successfull");
		verifyTrue(loginAdminPage.isChampionSuccessMessDisplay(DriverManager.getDriver(), "Champion account is created in the system."));
		
		log.info("Step TC_001_CreateClient - 20: Verify Champion Create successfull");
		verifyTrue(loginAdminPage.isResultDisplayedCorrectly(DriverManager.getDriver(), "Inactive"));
		
		log.info("Step TC_001_CreateClient - 21: Verify Champion Create successfull");
		verifyTrue(loginAdminPage.isResultDisplayedCorrectly(DriverManager.getDriver(), championFirstName));
		
		log.info("Step TC_001_CreateClient - 22: Verify Champion Create successfull");
		verifyTrue(loginAdminPage.isLastNameDisplayedCorrectly(DriverManager.getDriver(), championLastName));
		
		log.info("Step TC_001_CreateClient - 23: Verify Champion Create successfull");
		verifyTrue(loginAdminPage.isEmailDisplayedCorrectly(DriverManager.getDriver(), championEmail));
		
		log.info("Step TC_001_CreateClient - 24: Verify Champion Create successfull");
		verifyTrue(loginAdminPage.isResultDisplayedCorrectly(DriverManager.getDriver(), championPhoneNumber));
		
//		log.info("Step TC_001_CreateClient - 25: Verify Champion Create successfull");
//		verifyTrue(loginAdminPage.isResultDisplayedCorrectly(DriverManager.getDriver(), currentDate));
		
		log.info("Step TC_001_CreateClient - 26: Log out");
		loginAdminPage.logout();
		
		log.info("Step TC_001_CreateClient - 27: Open yopmail");
		loginAdminPage.openLink(DriverManager.getDriver(),"http://www.yopmail.com/en/");
		
		log.info("Step TC_001_CreateClient - 28: Input email");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "login", championEmail);
		
		log.info("Step TC_001_CreateClient - 29: Click on Check Inbox");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "Check Inbox");
					
		log.info("Step TC_001_CreateClient - 30: Open Register email");
		mailinatorPage.clickOnEmailTitle(DriverManager.getDriver(),"been invited to join the Art of Mentoring platform");
		
		log.info("Step TC_001_CreateClient - 31: Click Set password button");
		emaildriver = mailinatorPage.clickOnlinkInsideMail(DriverManager.getDriver(),"Set password");
		
		log.info("Step TC_001_CreateClient - 32: Input password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "password", password );
		
		log.info("Step TC_001_CreateClient - 33: Confirm password");
		loginAdminPage.inputSelecterTextfieldByID(DriverManager.getDriver(), "UserConfirmPassword", password );
		
		log.info("Step TC_001_CreateClient - 34: Select checkbox");
		loginAdminPage.clickOnElementByItsID(DriverManager.getDriver(), "keepSigned5");
		
		log.info("Step TC_001_CreateClient - 35: Click Login");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "LOG IN");
		
		log.info("Step TC_001_CreateClient - 36: Verify password updated");
		verifyTrue(loginAdminPage.isPassSuccessMessDisplay(DriverManager.getDriver(), "Your password has successfully been updated. Click Ok to login to your account."));
		
		log.info("Step TC_001_CreateClient - 37: Click OK button");
		loginAdminPage.clickOnElementByItsValue(DriverManager.getDriver(), "OK");
		loginAdminPage.sleep();
		
		verifyTrue(loginAdminPage.isChampionNameCorrect(DriverManager.getDriver(),userProfileName ));

				
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
//		closeBrowser();
	}

	private LoginAdminPage loginAdminPage;
	private MailinatorPage mailinatorPage;
	private WebDriver  emaildriver;

	String emailAdmin, password;
	String clientPage, clientName;
	String championFirstName, championLastName, championEmail, championPhoneNumber,currentDate;
	String userProfileName;

}