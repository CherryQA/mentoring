package common;

import org.openqa.selenium.WebDriver;

public class Constant {

	public static class PathConfig {
		public static final String AUTOMATION_CONFIG_XML = "configuration/automation.config.xml";
		public static final String DATA_TEST_XML = "configuration/dataTest.xml";
		public static final String CAPTURE_SCREENSHOT = "\\test-output\\screenshots";
		public static final String FOLDER_DOWNLOAD_ON_WIN = "C:\\Users\\%s\\Downloads\\";
		public static String HOME_URL = "https://staging.artofmentoring.net/";
		public static String GRID = "";
	}

	public static class LoginData {
		// Database:
		public static final String USERNAME = "cherry.seniorqa@gmail.com";
		public static final String PASSWORD = "ABCD1234";

	}

//	public static class MySQL {
//		public static final String URL = "";
//		public static final String USER_NAME = "";
//		public static final String PASSWORD = "";
//	}

	public static class MainNavigationTab {
		public static final String LOGOUT = "Logout";
	}

	public static WebDriver driver = null;

}
