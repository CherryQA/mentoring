package page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.openqa.jetty.html.Page;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import common.AutomationAction;
import common.Common;

public class AbstractPage extends AutomationAction {

	protected AbstractPage() {
		log = LogFactory.getLog(getClass());
		control.setPage(this.getClass().getSimpleName());
		log.debug("Created page abstraction for " + getClass().getName());
	}

	// =====================================Action
	// Methods===========================//

	public void openUrlFromUrlList(WebDriver driver, String baseUrl, String[] urlList) {
		System.out.println(urlList.length);
		for (String url : urlList) {
			if (!url.isEmpty())
				System.out.println("Link: " + baseUrl + url);
			openLink(driver, baseUrl + url);
			// else;
		}
	}

	/**
	 * Check alert message is displayed correctly
	 * 
	 * @param driver
	 * @param text
	 * @return
	 */
	public boolean isTextDisplayed(WebDriver driver, String text) {
		return isControlDisplayedWithLowerTimeOut(driver, mentoring.AbstractPage.dynamicText, text);
	}

	/**
	 * click Button by text
	 * 
	 * @param driver
	 */
	public void clickButtonByItsText(WebDriver driver, String text) {
		click(driver, mentoring.AbstractPage.dynamicRegularButtonByName, text);
		sleep(3);
	}

	/**
	 * click Link button by text
	 * 
	 * @param driver
	 */
	public void clickLinkButtonByItsText(WebDriver driver, String text) {
		click(driver, mentoring.AbstractPage.dynamicLinkButtonByName, text);
	}

	/**
	 * click Link by text
	 * 
	 * @param driver
	 */
	public void clickLinkByItsText(WebDriver driver, String text) {
		click(driver, mentoring.AbstractPage.dynamicLinkByLinkName, text);
		sleep(2);
	}

	/**
	 * click Link by text
	 * 
	 * @param driver
	 */
	public boolean isLinkByItsTextDisplayed(WebDriver driver, String text) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicLinkByLinkName, text);
	}

	/**
	 * click Link by text
	 * 
	 * @param driver
	 */
	public void clickLinkByItsText(WebDriver driver, String text, String index) {
		click(driver, mentoring.AbstractPage.dynamicLinkByLinkNameWithIndex, text, index);
		sleep(2);
	}

	/**
	 * Input textfield by textfield label
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputTextfieldByTextfieldLabel(WebDriver driver, String label, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByTextfieldLabel, text, label);
		sleep(2);
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputTextfieldByIDWithEnter(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByID, text, id);
		keyPressing("enter");
		keyPressing("enter");
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputSelecterTextfieldByID(WebDriver driver, String id, String text) {
		waitForControl(driver, mentoring.AbstractPage.dynamicTextFieldByID,timeout, text, id);
		type(driver, mentoring.AbstractPage.dynamicTextFieldByID, text, id);
		sleep(1);
//		if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text))
//			click(driver, mentoring.AbstractPage.dynamicDiv, text);
//		sleep(1);
	}

	public void inputSelecterTextfieldByID(WebDriver driver, String id, String text1, String text2) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByID, text1, id);
		sleep(3);
		if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text1))
			click(driver, mentoring.AbstractPage.dynamicDiv, text1);
		else {
			type(driver, mentoring.AbstractPage.dynamicTextFieldByID, text2, id);
			sleep(3);
			if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text2))
				click(driver, mentoring.AbstractPage.dynamicDiv, text2);
		}
		sleep(3);
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputSelecterTextfieldByName(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByName, text, id);
		sleep(3);
		if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text))
			click(driver, mentoring.AbstractPage.dynamicDiv, text);
		sleep(1);
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputTextfieldByID(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByID, text, id);
	}

	/**
	 * Input textarea by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputTextareaByID(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextAreaByID, text, id);
	}

	/**
	 * Get textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public String getTextfieldByID(WebDriver driver, String id) {
		return getAttributeValue(driver, mentoring.AbstractPage.dynamicTextFieldByID, "value", id);
	}

	/**
	 * Get textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public String getTextareaByID(WebDriver driver, String id) {
		System.out.println("text: " + getText(driver, mentoring.AbstractPage.dynamicTextAreaByID, id));
		return getText(driver, mentoring.AbstractPage.dynamicTextAreaByID, id);
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputTextfieldByName(WebDriver driver, String name, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByName, text, name);
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public String getTextfieldByName(WebDriver driver, String name) {
		return getAttributeValue(driver, mentoring.AbstractPage.dynamicTextFieldByName, "value", name);
	}

	public void clickOnClearButton(WebDriver driver) {
		click(driver, mentoring.AbstractPage.clearButton);
		sleep(3);
	}

	public void clickOnSearchButton(WebDriver driver) {
		click(driver, mentoring.AbstractPage.searchButton);
		sleep(3);
	}

	public void selectItemFromDropdownByID(WebDriver driver, String id, String item) {
		sleep();
		selectComboxboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByID, item, id);
		sleep();
	}

	public String getSelectedItemByID(WebDriver driver, String id) {
		return getSelectedComboboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByID, id);
	}

	public void selectItemFromDropdownByClass(WebDriver driver, String className, String item) {
		selectComboxboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByClass, item, className);
		sleep();
	}

	public String getSelectedItemByClass(WebDriver driver, String className) {
		return getSelectedComboboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByClass, className);
	}

	public void selectItemFromDropdownByName(WebDriver driver, String name, String item) {
		selectComboxboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByName, item, name);
		sleep();
	}

	public String getSelectedItemByName(WebDriver driver, String name) {
		return getSelectedComboboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByName, name);
	}

	public boolean isTableByIdContainsText(WebDriver driver, String id, String item) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicTableByIDContainsText, id, item);
	}

	public String getPageTitle(WebDriver driver) {
		sleep(2);
		return driver.getTitle();
	}

	public void clickOnElementByItsValue(WebDriver driver, String value) {
		forceClick(driver, mentoring.AbstractPage.dynamicElementByValue, value);
//		click(driver, mentoring.AbstractPage.dynamicElementByValue, value);
		sleep(2);
	}

	public void clickOnElementByItsValue(WebDriver driver, String value, String index) {
		click(driver, mentoring.AbstractPage.dynamicElementByValueIndex, value, index);
		sleep(2);
	}

	public void clickOnElementByItsName(WebDriver driver, String value) {
		click(driver, mentoring.AbstractPage.dynamicElementByName, value);
		sleep(2);
	}

	public void clickOnElementByItsID(WebDriver driver, String id) {
		waitForControl(driver, mentoring.AbstractPage.dynamicElementByID,timeout, id );
		forceClick(driver, mentoring.AbstractPage.dynamicElementByID, id);
		sleep(2);
	}
	public void clickOnElementByItsClass(WebDriver driver, String id) {
		waitForControl(driver, mentoring.AbstractPage.dynamicElementByClass,timeout, id );
		forceClick(driver, mentoring.AbstractPage.dynamicElementByClass, id);
		sleep(2);
	}

	public boolean isElementByItsIDContainsText(WebDriver driver, String id, String text) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicElementByIDContainsText, id, text);
	}

	public void forceClickOnElementByItsID(WebDriver driver, String id) {
		forceClick(driver, mentoring.AbstractPage.dynamicElementByID, id);
		sleep(2);
	}

	public void clickOnElementByItsText(WebDriver driver, String id) {
		click(driver, mentoring.AbstractPage.dynamicElementByText, id);
		sleep(2);
	}

	public void clickOnElementByItsText(WebDriver driver, String id, String index) {
		click(driver, mentoring.AbstractPage.dynamicElementByTextWithIndex, id, index);
		sleep(2);
	}

	public void clickOnElementByItsTitle(WebDriver driver, String title) {
		click(driver, mentoring.AbstractPage.dynamicElementByTitle, title);
		sleep(2);
	}

	public void clickOnDivByItsText(WebDriver driver, String divName) {
		click(driver, mentoring.AbstractPage.dynamicDiv, divName);
		sleep(3);
	}

	public void clickOnCheckboxByName(WebDriver driver, String checkboxName) {
		click(driver, mentoring.AbstractPage.dynamicCheckboxByItsName, checkboxName);
		sleep(2);
	}

	public String getAlertText(WebDriver driver) {
		return getTextJavascriptAlert(driver);
	}

	public void acceptAlert(WebDriver driver) {
		if (isAlertPresent(driver))
			acceptJavascriptAlert(driver);
	}

	public void acceptAlertByMessage(WebDriver driver, String message) {
		if (getAlertText(driver).contains(message))
			acceptJavascriptAlert(driver);
	}

	public void clickOnImageButtonByItsSrc(WebDriver driver, String src) {
		click(driver, mentoring.AbstractPage.dynamicImgByItsSrc, src);
		sleep(3);
	}

	public void clickOnImageButtonByItsSrc(WebDriver driver, String src, String index) {
		click(driver, mentoring.AbstractPage.dynamicImgByItsSrcWithIndex, src, index);
		sleep(3);
	}

	public void clickOnLinkByPreciselyText(WebDriver driver, String linkName) {
		click(driver, mentoring.AbstractPage.dynamicLinkByLinkNamePrecisely, linkName);
		sleep(2);
	}

	public boolean isLinkNameDisplayed(WebDriver driver, String linkName) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicLinkByLinkNamePrecisely, linkName);
	}

	public String getElementAttributeByID(WebDriver driver, String id, String value) {
		return getAttributeValue(driver, mentoring.AbstractPage.dynamicElementByID, value, id);
	}

	public String getElementTextByID(WebDriver driver, String id) {
		return getText(driver, mentoring.AbstractPage.dynamicElementByID, id);
	}

	public void selectRadioButtonByName(WebDriver driver, String radioButtonName) {
		click(driver, mentoring.AbstractPage.dynamicRadioButtonByName, radioButtonName);
		sleep(2);
	}

	public boolean isImageButtonDisplayed(WebDriver driver, String src) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicImgByItsSrc, src);
	}

	public boolean isItemInSelectedList(WebDriver driver, String id, String itemName) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicSelectedListByID, id, itemName);
	}

	public boolean isSuggestionDropdownDisplayedCorrectly(WebDriver driver, String id, String value) {
		return getAttributeValue(driver, mentoring.AbstractPage.dynamicSuggestionDropdownByID, "innerHTML", id).contains(value);
	}

	public boolean isSuggestionDropdownDisplayedCorrectlyByScript(WebDriver driver, String id, String value) {
		return getAttributeValue(driver, mentoring.AbstractPage.dynamicDropdownScriptByID, "innerHTML", id).contains(value);
	}

	public String getElementTextByClass(WebDriver driver, String className) {
		return getText(driver, mentoring.AbstractPage.dynamicElementByClass, className);
	}

	public boolean isResultTableContainsRecord(WebDriver driver, String value1, String value2) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicResultTableContainsText, value1, value2);
	}

//	public void uploadFile(WebDriver driver, String uploadButtonID, String fileName) {
//		upload(driver, mentoring.AbstractPage.UploadButton, getPathFile("resource/file/" + fileName), uploadButtonID);
//		sleep(3);
//	}
	public void uploadFile(WebDriver driver, String fileName) {
		upload(driver, mentoring.AbstractPage.UploadButton, getPathFile("/resource/data upload/" + fileName));
		sleep(3);
	}

	public void openTab(WebDriver driver, String id) {
		sleep(3);
		click(driver, mentoring.AbstractPage.dynamicTab, id);
		sleep(2);
	}

	public String getPdfText() {
		// type(driver, epmxweb.MailinatorPage.messageBody, message);
		sleep();
		Robot robot;
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_A);
			robot.keyRelease(KeyEvent.VK_A);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_C);
			robot.keyRelease(KeyEvent.VK_C);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(getClipboardData());
		return getClipboardData();
	}

	// public void addRunID() {
	// Common.getCommon().addRunID();
	// }

	public String getFileName(String shortName) {
		return Common.getCommon().getFileName(shortName);
	}

	/**
	 * wait For Download File Fullname Completed
	 * 
	 */
	public void waitForDownloadFileFullnameCompleted(String fileName) {
		int i = 0;
		while (i < 30) {
			boolean exist = Common.getCommon().isFileExists(fileName);
			if (exist == true) {
				i = 30;
			}
			sleep(1);
			i = i + 1;
		}
	}

	/**
	 * wait For Download File Contains name Completed
	 * 
	 */
	public void waitForDownloadFileContainsNameCompleted(String fileName) {
		int i = 0;
		while (i < 30) {
			boolean exist = Common.getCommon().isFileContains(fileName);
			if (exist == true) {
				i = 30;
			}
			sleep(1);
			i = i + 1;
		}
	}

	/**
	 * delete file full name in folder
	 * 
	 */
	public void deleteFileFullName(String fileName) {
		if (Common.getCommon().isFileExists(fileName)) {
			Common.getCommon().deleteFileFullName(fileName);
		}
	}

	/**
	 * delete file contains name in folder
	 * 
	 */
	public void deleteContainsFileName(String fileName) {
		Common.getCommon().deleteFileContainsName(fileName);
	}

	/**
	 * count number of file in folder
	 * 
	 */
	public int countFilesInDirectory() {
		return Common.getCommon().countFilesInDirectory();
	}

	/**
	 * Get filename in directory
	 * 
	 * @return
	 */
	public String getFileNameInDirectory() {
		return Common.getCommon().getFileNameInDirectory();
	}

	/**
	 * delete all files in folder
	 * 
	 */
	public void deleteAllFileInFolder() {
		Common.getCommon().deleteAllFileInFolder();
	}

	/**
	 * check file exist
	 * 
	 */
	public boolean isFileExist(String fileName) {
		waitForDownloadFileFullnameCompleted(fileName);
		if (Common.getCommon().isFileExists(fileName)) {
			return true;
		} else
			return false;
	}

	/**
	 * check file contains
	 */
	public boolean isFileContain(String fileName) {
		waitForDownloadFileContainsNameCompleted(fileName);
		if (Common.getCommon().isFileContains(fileName)) {
			return true;
		} else
			return false;
	}

	public void closeOtherWindows(WebDriver driver) {
		String currentWindows = switchWindow(driver);
		switchWindowBackAndCloseCurrentWindow(driver, currentWindows);
	}

	public boolean isRejectInfoDisplayed(WebDriver driver, String infoLabel, String infoValue) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicRejectInfo, infoLabel, infoValue);
	}

	public boolean isInfoDialogValueDisplayed(WebDriver driver, String dialogID, String value) {
		return isControlDisplayed(driver, mentoring.AbstractPage.dynamicInfoDialog, dialogID, value);
	}

	/**
	 * Check CSV file exported correctly
	 * 
	 * @param csvFilePath
	 * @param text
	 * @return
	 */
	public Boolean isCSVFileExportedCorrectly(String csvFilePath, String text, String collumnName) {
		return Common.getCommon().isTextDisplayedInCSVFile(csvFilePath, text, collumnName);
	}

	public void clickOnApproveButton(WebDriver driver, String text, String value) {
		click(driver, mentoring.AbstractPage.approvexpath, text, value);
		sleep(3);
	}

	/**
	 * Input textfield by textfield id
	 * 
	 * @param driver
	 * @param label
	 * @param text
	 */
	public void inputSelectTextFieldByXpath(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByID, text, id);
		sleep(3);
		if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text))
			click(driver, mentoring.AbstractPage.dynamicDiv, text);
		sleep(3);
	}
	
	
	public void inputSelecterTextfieldByColumn(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByColumn, text, id);
		sleep(3);
//		if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text))
//			click(driver, mentoring.AbstractPage.dynamicDiv, text);
//		sleep(3);
	}
	public void inputSelecterTextfieldByContains(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicTextFieldByContains, id , text);
		sleep(3);
//		if (isControlDisplayed(driver, mentoring.AbstractPage.dynamicDiv, text))
//			click(driver, mentoring.AbstractPage.dynamicDiv, text);
//		sleep(3);
	}
	
	public void inputSelecterTextfieldByClass(WebDriver driver, String id, String text) {
		type(driver, mentoring.AbstractPage.dynamicElementByClass, text, id);
		sleep(3);
	}
	public void selectItemFromDropdownByClassAndIndex(WebDriver driver, String className, String item) {
		selectComboxboxItem(driver, mentoring.AbstractPage.dynamicSelectFieldByClassNumber1, item, className);
		sleep();
	}
	
	public String getTextfieldByClass(WebDriver driver, String id) {
		return getAttributeValue(driver, mentoring.AbstractPage.dynamicElementByClass, "value", id);
	}
	
	public void uploadImage(WebDriver driver, String fileName) throws AWTException {
		
	    //put path to your image in a clipboard
	    StringSelection ss = new StringSelection("D:\\Project\\Automation\\MENTORING_MASTER\\resource\\data upload\\" + fileName);
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

	    //imitate mouse events like ENTER, CTRL+C, CTRL+V
	    Robot robot = new Robot();
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.keyRelease(KeyEvent.VK_ENTER);
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
	protected final Log log;
	private String ipClient;
}
