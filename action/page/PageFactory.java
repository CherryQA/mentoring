package page;

import org.openqa.selenium.WebDriver;

public class PageFactory {


	/**
	 * get ConfigPage
	 * 
	 * @param driver
	 * @param ipClient
	 * @return ConfigPage object
	 */
	public static ConfigPage getConfigPage(WebDriver driver, String ipClient) {
		return new ConfigPage(driver, ipClient);
	}

	/**
	 * get MailinatorPage
	 * 
	 * @param driver
	 * @param ipClient
	 * @return MailinatorPage object
	 */
	public static MailinatorPage getMailinatorPage(WebDriver driver, String ipClient) {
		return new MailinatorPage(driver, ipClient);
	}
		/**
	 * get LoginPage
	 * 
	 * @param driver
	 * @param ipClient
	 * @return LoginPage object
	 */
	public static LoginAdminPage getLoginAdminPage(WebDriver driver, String ipClient) {
		return new LoginAdminPage(driver, ipClient);
	}

		public static ProgramPage getProgramPage(WebDriver driver, String ipClient) {
			return new ProgramPage(driver, ipClient);
		}

}
