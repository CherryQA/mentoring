package page;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import common.AutomationAction;
import common.Common;
import common.DriverManager;

public class LoginAdminPage extends AbstractPage {

	public LoginAdminPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	// ==============================Action Methods===========================//
	/**
	 * Login with emailAddress & password
	 * 
	 * @param emailAddress
	 * @param password
	 */
	public void login(String emailAddress, String password) {
		sleep(2);
		type(driver, mentoring.LoginAdminPage.adminEmailField, emailAddress);
		type(driver, mentoring.LoginAdminPage.adminPasswordField, password);
		click(driver, mentoring.LoginAdminPage.adminRememberCheckbox);
		click(driver, mentoring.LoginAdminPage.adminLoginButton);
	}

	public void logout() {
		click(driver, mentoring.LoginAdminPage.profileDropdown);
		click(driver, mentoring.LoginAdminPage.logoutButton);
		click(driver, mentoring.LoginAdminPage.yesButton);
		sleep(2);
	}

	/**
	 * check Error Message Display On Tooltip With Content
	 * 
	 * @param content
	 * @return true/false
	 */
	// public boolean isErrorMessageDisplayOnTooltipWithContent(String content){
	// JavascriptExecutor js = (JavascriptExecutor)driver;
	// WebElement field = driver.findElement(epmxweb.LoginPage.usernameTextbox);
	// String message = (String)js.executeScript("return
	// arguments[0].validationMessage;", field);
	// return message.contains(content);
	// }

	/**
	 * check email address textbox display
	 * 
	 * @return true/false
	 */
	// public boolean isEmailAddressTextboxDisplay(){
	// return isControlDisplayed(driver, epmxweb.LoginPage.usernameTextbox);
	// }

	public String getAlertText() {
		return getTextJavascriptAlert(driver);
	}

	public void acceptAlert() {
		if (isAlertPresent(driver))
			acceptJavascriptAlert(driver);
	}

	public boolean isClientSuccessMessDisplay(WebDriver driver2, String string) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.createClientSuccessful);
	}

	public boolean isChampionSuccessMessDisplay(WebDriver driver2, String string) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.createClientSuccessful);

	}

	public boolean isPassSuccessMessDisplay(WebDriver driver2, String string) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.setPassSuccessful);
	}

	public boolean isResultDisplayedCorrectly(WebDriver driver2, String string) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.dynamicResultSelecter, string);
	}

	public boolean isChampionNameCorrect(WebDriver driver2, String userProfileName) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.championNameSuccessful);
	}

	public boolean isLastNameDisplayedCorrectly(WebDriver driver2, String championLastName) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.dynamicResultSelecterLastName, championLastName);
	}

	public boolean isEmailDisplayedCorrectly(WebDriver driver2, String email) {
		return isControlDisplayed(driver, mentoring.AdminClientPage.dynamicResultSelecterEmail, email);
	}

	public String GetDateinJava() {

		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy ");

		// get current date time with Date()
		Date date = new Date();

		// Now format the date
		String date1 = dateFormat.format(date);
		return date1;

	}

	public void createNewClient(String clientName) {
		openLink(driver, "https://staging.artofmentoring.net/admin/clients");
		sleep(2);
		click(driver, mentoring.AdminClientPage.clientCreateNewButton);
		inputTextfieldByID(DriverManager.getDriver(), "clientname", clientName);
		selectItemFromDropdownByID(DriverManager.getDriver(), "ClientIndexed", "Yes");
		click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateInPopUp);
		click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateConfirmButton);
	}

	public void createNewChampion(String clientName, String championFirstName, String championLastName,
			String championEmail, String championPhoneNumber) {
		openLink(DriverManager.getDriver(), "https://staging.artofmentoring.net/admin/clients/");
		inputSelecterTextfieldByColumn(DriverManager.getDriver(), "0", clientName);
		click(DriverManager.getDriver(), mentoring.AdminClientPage.clientViewButton);
		click(DriverManager.getDriver(), mentoring.AdminClientPage.clientChampionButton);
		click(DriverManager.getDriver(), mentoring.AdminClientPage.clientCreateNewButton);
		sleep(1);
		inputSelecterTextfieldByID(DriverManager.getDriver(), "firstname", championFirstName);
		inputSelecterTextfieldByID(DriverManager.getDriver(), "lastname", championLastName);
		inputSelecterTextfieldByID(DriverManager.getDriver(), "email", championEmail);
		inputSelecterTextfieldByID(DriverManager.getDriver(), "phone", championPhoneNumber);
		clickOnElementByItsValue(DriverManager.getDriver(), "Create");
		if (isControlDisplayed(driver, mentoring.AdminClientPage.championIsExist,
				"Your email has been used for an existing account.")) {
			openLink(DriverManager.getDriver(), "https://staging.artofmentoring.net/admin/clients/");

		} else {
			logout();
			openLink(DriverManager.getDriver(), "http://www.yopmail.com/en/");
			inputSelecterTextfieldByID(DriverManager.getDriver(), "login", championEmail);
			clickOnElementByItsValue(DriverManager.getDriver(), "Check Inbox");

			switchToFrame(driver, mentoring.MailinatorPage.inboxiFrame);
			click(driver, mentoring.MailinatorPage.dynamicYopMailTitle,
					"been invited to join the Art of Mentoring platform");
			switchToTopWindowFrame(driver);

			String copylink = getText(DriverManager.getDriver(), mentoring.AdminClientPage.copyLink);
			openLink(DriverManager.getDriver(), copylink);
		}

	}

	private WebDriver driver;
	private String ipClient;
	
	public boolean isEmailisMissing() {
		return isControlDisplayed(driver, mentoring.AdminClientPage.emailIsMissing);
	}

	public boolean isPassWordIsMissing() {
		return isControlDisplayed(driver, mentoring.AdminClientPage.passwordIsMissing);
	}

	public boolean isEmailInvalid() {
		return isControlDisplayed(driver, mentoring.AdminClientPage.emailInvalid);
	}

	public boolean isPasswordInvalid() {
		return isControlDisplayed(driver, mentoring.AdminClientPage.passwordInvalid);
	}

	public boolean isProgrameCreated() {
		return isControlDisplayed(driver, mentoring.AdminClientPage.isProgramCreated);
	}
	
	public boolean isProgrameNameDisPlayedCorrect() {
		return isControlDisplayed(driver, mentoring.AdminClientPage.isProgrameNameDisPlayedCorrect);
	}
}
