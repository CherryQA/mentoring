package page;

import org.openqa.selenium.WebDriver;

public class ProgramPage extends AbstractPage{

	public ProgramPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}
	
	public boolean isParticipantCreatedSuccessful() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.participantCreatedSuccessful);
	}
	

	public void logout() {
		click(driver, mentoring.LoginAdminPage.profileDropdown);
		click(driver, mentoring.LoginAdminPage.logoutButton);
		click(driver, mentoring.LoginAdminPage.yesButton);
		sleep(2);
	}
	
	
	private WebDriver driver;
	private String ipClient;
	public boolean isPariticpantNameDisplayedCorrect(String name) {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantNameDisplayedCorrect);
	}


	public boolean isParticipantStatusDisplayedCorrect() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantStatusDisplayedCorrect);
	}

	public boolean isParticipantRoleDisplayedCorrect() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantRoleDisplayedCorrect);

	}

	public boolean isParticipantAddDateDisplayedCorrect() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantAddDateDisplayedCorrect);

	}

	public boolean isParticipantMentorTrainingStatussDisplayedCorrect() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantMentorTrainingStatussDisplayedCorrect);

	}

	public boolean isParticipantMenteeTrainingStatussDisplayedCorrect() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantMenteeTrainingStatussDisplayedCorrect);

	}

	public boolean isParticipantEmailNotifyDisplayedCorrect() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isParticipantEmailNotifyDisplayedCorrect);

	}

	public boolean isApplicationNotifyDisplayed() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isApplicationNotifyDisplayed);

	}

	public boolean isMentorApplicationTileDisplayed() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isMentorApplicationTileDisplayed);

	}
	public boolean isMenteeApplicationTileDisplayed() {
		return isControlDisplayed(driver, mentoring.ChampionParticipantPage.isMenteeApplicationTileDisplayed);
		
	}
	
}
