package page;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.WebDriver;

import common.DriverManager;

public class MailinatorPage extends AbstractPage {

	public MailinatorPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	// ==============================Action Methods===========================//

	/**
	 * Open Mailbox * @param emailAddress
	 */
	public void openMailbox(String emailAddress) {
		type(driver, mentoring.MailinatorPage.mailAddressTextfield, emailAddress);
		click(driver, mentoring.MailinatorPage.goButton);
		sleep(3);
	}

	/**
	 * Open email by it's title
	 * 
	 * @param emailTitle
	 */
	public void openMailByTitle(String emailTitle) {
		click(driver, mentoring.MailinatorPage.dynamicMailLink, emailTitle);
		sleep(3);
	}

	/**
	 * Open email by it's title
	 * 
	 * @param emailTitle
	 */
	public void openResetPasswordPage() {
		switchToFrame(driver, mentoring.MailinatorPage.mailFrame);
		clickLinkButtonByItsText(driver, "Set New Password");
		switchToTopWindowFrame(driver);
		sleep(3);
	}

	/**
	 * Open email by it's title
	 * 
	 * @param emailTitle
	 */
	public void openNewPasswordPage() {
		switchToFrame(driver, mentoring.MailinatorPage.mailFrame);
		clickLinkByItsText(driver, "New Password.");
		switchToTopWindowFrame(driver);
		sleep(3);
	}

	public void inputEmailAddress(String emailAddress) {
		type(driver, mentoring.MailinatorPage.emailAdress, emailAddress);
	}

	public void inputPassword(String password) {
		type(driver, mentoring.MailinatorPage.password, password);
	}

	public void inputRecipient(String recipient) {
		type(driver, mentoring.MailinatorPage.recipientTextarea, recipient);
	}

	public void inputSubject(String subject) {
		type(driver, mentoring.MailinatorPage.subjectBox, subject);
	}

	// public void inputMessage(String message){
	//// type(driver, epmxweb.MailinatorPage.messageBody, message);
	// click(driver, epmxweb.MailinatorPage.messageBody);
	// sleep();
	// setClipboardData(message);
	// Robot robot;
	// try {
	// robot = new Robot();
	// robot.keyPress(KeyEvent.VK_CONTROL);
	// robot.keyPress(KeyEvent.VK_V);
	// robot.keyRelease(KeyEvent.VK_V);
	// robot.keyRelease(KeyEvent.VK_CONTROL);
	// } catch (AWTException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	public void clickOnNextButton() {
		click(driver, mentoring.MailinatorPage.nextButton);
		sleep();
	}

	public void clickOnSignInButton() {
		click(driver, mentoring.MailinatorPage.signInButton);
		sleep();
	}

	public void clickOnComposeButton() {
		click(driver, mentoring.MailinatorPage.composeButton);
		sleep();
	}

	public void clickOnAttachFileButton() {
		click(driver, mentoring.MailinatorPage.attachFile);
		sleep();
	}

	public void clickOnSendButton() {
		click(driver, mentoring.MailinatorPage.sendButton);
		sleep();
	}

	public void loginGmail(String emailAddress, String emailPassword) {
		inputEmailAddress(emailAddress);
		clickOnNextButton();
		inputPassword(emailPassword);
		clickOnSignInButton();
	}

	// public void sendEmail(String recipient, String subject, String message,
	// String fileName){
	// clickOnComposeButton();
	// inputRecipient(recipient);
	// inputSubject(subject);
	// inputMessage(message);
	// clickOnAttachFileButton();
	// setClipboardData(fileName);
	// Robot robot;
	// try {
	// robot = new Robot();
	// robot.keyPress(KeyEvent.VK_CONTROL);
	// robot.keyPress(KeyEvent.VK_V);
	// robot.keyRelease(KeyEvent.VK_V);
	// robot.keyRelease(KeyEvent.VK_CONTROL);
	// robot.keyPress(KeyEvent.VK_ENTER);
	// robot.keyRelease(KeyEvent.VK_ENTER);
	// } catch (AWTException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// sleep(10);
	// clickOnSendButton();
	// sleep(10);
	// }

	public void expandAllTrimmed() {
		clickAllElement(driver, mentoring.MailinatorPage.expandButton);
		sleep();
	}

	public void deleteEmail() {
		click(driver, mentoring.MailinatorPage.deleteEmail);
		sleep();
	}

	// public void openYopmailByTitle(WebDriver driver2, String emailTitle){
	// click(driver, mentoring.MailinatorPage.dynamicYopMailTitle, emailTitle);
	// sleep(1);
	// }

	public WebDriver clickOnlinkInsideMail(WebDriver driver, String button) {
		switchToFrame(driver, mentoring.MailinatorPage.yopmailFrame);
		clickOnElementByItsText(driver, "%s");

		// switchToTopWindowFrame(driver);
		sleep(2);

		// To switch to the new tab
		Set<String> handles = driver.getWindowHandles();
		String currentWindowHandle = driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		for (String handle : handles) {
			if (!currentWindowHandle.equals(handle)) {
				driver.switchTo().window(handle);
			}
		}

		return driver;
	}

	public void clickOnEmailTitle(WebDriver driver2, String emailTitle) {
		switchToFrame(driver, mentoring.MailinatorPage.inboxiFrame);
		click(driver, mentoring.MailinatorPage.dynamicYopMailTitle, emailTitle);
		switchToTopWindowFrame(driver);
		sleep(1);
	}

	private WebDriver driver;
	private WebDriver driver2 = null;
	private String ipClient;
}
