package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.Constant;

public class ConfigPage extends AbstractPage {

	public ConfigPage(WebDriver driver, String ipClient) {
		control.setPage(this.getClass().getSimpleName());
		this.driver = driver;
		this.ipClient = ipClient;
	}

	// ==============================Action Methods===========================//

	public String getAlertText() {
		return getTextJavascriptAlert(driver);
	}

	public void acceptAlert() {
		if (isAlertPresent(driver))
			acceptJavascriptAlert(driver);
	}

	// public void changeUserClassSubModule(String userClass, String tab, String
	// optionName, String optionValue){
	// openLink(driver,
	// "https://cherry.epmxweb.com/utilities/userclass_submodule_access.php");
	// inputTextfieldByID(driver, "txt_Mp", "");
	// clickOnElementByItsID(driver, "btn_MasterPassOk");
	// sleep(3);
	// inputSelecterTextfieldByID(driver, "txt_UserClass", userClass);
	// sleep(3);
	// click(driver, mobixie.ConfigPage.dynamicAccessTab, tab);
	// click(driver, mobixie.ConfigPage.dynamicAccessOption, optionName,
	// optionValue);
	// clickOnImageButtonByItsSrc(driver, "save.gif");
	// }
	private WebDriver driver;
	private String ipClient;
}
